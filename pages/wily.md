<!--
.. title: Download Ubuntu MATE 15.10
.. slug: wily
.. date: 2014-06-24 18:01:09 UTC
.. tags: Ubuntu,MATE,wily,15.10,download
.. link:
.. description:
.. type: text
.. author: Martin Wimpress
-->

The Ubuntu MATE .iso image allows you to try Ubuntu MATE without changing your
computer at all, and at your option to install it permanently later. You will
need at least 512MB of RAM to install from this image.

<div class="bs-component">
    <div class="jumbotron">
        <h1>Release announcement</h1>
        <p>Find out what changed in Ubuntu MATE 15.10</p>
        <a href="/blog/ubuntu-mate-wily-final-release/" class="btn btn-primary btn-lg">Release announcement</a>
        </p>
    </div>
</div>

## Download

Ubuntu MATE is currently available for four architectures, PC (Intel x86),
64-bit PC (AMD64), Mac (PowerPC) and IBM-PPC (POWER5) and
Raspberry Pi 2 aarch32 (ARMv7).

  * **PC (Intel x86)** For almost all PCs. This includes most machines with Intel/AMD/etc type processors and almost all computers that run Microsoft Windows, as well as newer Apple Macintosh systems based on Intel processors. Choose this if you are at all unsure.
  * **64-bit PC (AMD64)** Choose this to take full advantage of computers based on the AMD64 or EM64T architecture (e.g., Athlon64, Opteron, EM64T Xeon, Core 2). If you have a non-64-bit processor made by AMD, or if you need full support for 32-bit code, use the Intel x86 images instead.
  * **Mac (PowerPC) and IBM-PPC (POWER5)** For Apple Macintosh G3, G4, and G5 computers, including iBooks and PowerBooks as well as older IBM OpenPower 7xx machines.

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component text-center">
      <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-i386.iso.torrent">
        <img src="/assets/img/misc/torrent.png" alt="Ubuntu MATE 15.10 PC (Intel x86) Download" title="Ubuntu MATE 15.10 PC (Intel x86) Download" />
      </a>
      <p>Ubuntu MATE 15.10 Torrent</p><p><b>PC (Intel x86)</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
        <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-amd64.iso.torrent">
        <img src="/assets/img/misc/torrent.png" alt="Ubuntu MATE 15.10 64-bit PC (AMD64) Download" title="Ubuntu MATE 15.10 64-bit PC (AMD64) Download" />
      </a>
      <p>Ubuntu MATE 15.10 Torrent</p><p><b>64-bit PC (AMD64)</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
      <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-powerpc.iso.torrent">
        <img src="/assets/img/misc/torrent.png" alt="Ubuntu MATE 15.10 Mac (PowerPC) and IBM-PPC (POWER5) Download" title="Ubuntu MATE 15.10 Mac (PowerPC) and IBM-PPC (POWER5) Download" />
      </a>
      <p>Ubuntu MATE 15.10 Torrent</p><p><b>Mac (PowerPC) and IBM-PPC (POWER5)</b></p>
    </div>
  </div>
</div>

## Download tip

<img class="right" src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-100px.png" alt="PayPal Logo">
If everyone who downloaded Ubuntu MATE donated **$2.50** it would
fund the full-time development of Ubuntu MATE *and* MATE
Desktop. <u>Please give us a tip and help both projects flourish!</b> If
you'd [like to donate more or become an Ubuntu MATE patron](/donate/)
please visit the [donate page](/donate/).</p>

<div class="row">
  <div class="col-lg-3">
    <div class="well bs-component" align="center">
      <form name="single" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <fieldset>
          <button type="submit" class="btn btn-primary">Tip us <b>$2.50</b></button>
        </fieldset>
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="6282B4CZGVCB6">
        <input type="hidden" name="item_name" value="Ubuntu MATE 15.10 Download Tip">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="charset" value="UTF-8">
        <input type="hidden" name="amount" value="2.50">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="src" value="1">
        <input type="hidden" name="sra" value="1">
        <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
        <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
      </form>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component" align="center">
      <form name="single" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <fieldset>
            <button type="submit" class="btn btn-primary">Tip us <b>$5.00</b></button>
        </fieldset>
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="6282B4CZGVCB6">
        <input type="hidden" name="item_name" value="Ubuntu MATE 15.10 Download Tip">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="charset" value="UTF-8">
        <input type="hidden" name="amount" value="5.00">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="src" value="1">
        <input type="hidden" name="sra" value="1">
        <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
        <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
      </form>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component" align="center">
      <form name="single" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <fieldset>
          <button type="submit" class="btn btn-primary">Tip us <b>$10.00</b></button>
        </fieldset>
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="6282B4CZGVCB6">
        <input type="hidden" name="item_name" value="Ubuntu MATE 15.10 Download Tip">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="charset" value="UTF-8">
        <input type="hidden" name="amount" value="10.00">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="src" value="1">
        <input type="hidden" name="sra" value="1">
        <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
        <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
      </form>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component" align="center">
      <form name="single" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <fieldset>
          <button type="submit" class="btn btn-primary">Tip us <b>$20.00</b></button>
        </fieldset>
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="6282B4CZGVCB6">
        <input type="hidden" name="item_name" value="Ubuntu MATE 15.10 Download Tip">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="charset" value="UTF-8">
        <input type="hidden" name="amount" value="20.00">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="src" value="1">
        <input type="hidden" name="sra" value="1">
        <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
        <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
      </form>
    </div>
  </div>
</div>

## HTTP direct download

In addition to the recommended BitTorrent downloads above, the .iso images can
also be downloaded via HTTP.

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component text-center">
      <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-i386.iso">
        <img src="/assets/img/misc/iso-dvd-cd-disc.png" alt="Ubuntu MATE 15.10 PC (Intel x86) Download" title="Ubuntu MATE 15.10 PC (Intel x86) Download" />
      </a>
      <p>Ubuntu MATE 15.10</p><p><b>PC (Intel x86)</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
        <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-amd64.iso">
          <img src="/assets/img/misc/iso-dvd-cd-disc.png" alt="Ubuntu MATE 15.10 64-bit PC (AMD64) Download" title="Ubuntu MATE 15.10 64-bit PC (AMD64) Download" />
        </a>
      <p>Ubuntu MATE 15.10</p><p><b>64-bit PC (AMD64)</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
      <a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/ubuntu-mate-15.10-desktop-powerpc.iso">
        <img src="/assets/img/misc/iso-dvd-cd-disc.png" alt="Ubuntu MATE 15.10 Mac (PowerPC) and IBM-PPC (POWER5) Download" title="Ubuntu MATE 15.10 Mac (PowerPC) and IBM-PPC (POWER5) Download" />
      </a>
      <p>Ubuntu MATE 15.10</p><p><b>Mac (PowerPC) and IBM-PPC (POWER5)</b></p>
    </div>
  </div>
</div>

If you direct download the .iso image please make sure the [appropriate
MD5 hash](http://cdimage.ubuntu.com/ubuntu-mate/releases/15.10/release/MD5SUMS) matches.

### Purchase DVDs and USBs

[OSDisc.com](https://www.osdisc.com/products/ubuntumate?affiliate=ubuntumate)
is the leading source for Linux DVDs and USBs, serving the Linux community
for over 10 years. Purchase ready-to-use bootable DVDs and
[8GB - 64GB DataTraveler SE9 G2 USB 3.0](http://www.kingston.com/en/usb/personal_business#dtse9g2)
memory sticks that come pre-installed with Ubuntu MATE and have persistent storage<i>*</i>.
<b>Carry Ubuntu MATE 15.10, your applications and your data on a USB stick that you can boot
on any USB equipped PC.</b>

<div class="row">
  <div class="col-lg-12">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.osdisc.com/">OSDisc.com</a>
        <a class="list-group-item" href="https://www.osdisc.com/products/ubuntumate?affiliate=ubuntumate">
        <img class="centered" src="/images/sponsors/osdisc.png" alt="OSDisc.com" /></a>
      </div>
    </div>
  </div>
</div>

<small><i>* Persistent storage is only available on USB sticks that are 32GB or larger</i></small>


### Mirrors

You might prefer to find a CD image mirror server that is geographically close
to you in order to achieve a faster download.

  * [Official CD Mirrors for Ubuntu](https://launchpad.net/ubuntu/+cdmirrors)

The image can be burned to a DVD, mounted as an ISO file, or be directly
written to a USB stick using a utility like `dd` or `ddrescue` (from the
`gddrescue` package), for example:

    sudo ddrescue -d -D --force ubuntu-mate-15.10-desktop-amd64.iso /dev/sdx
    sudo ddrescue -d -D --force ubuntu-mate-15.10-desktop-i386.iso /dev/sdx
    sudo ddrescue -d -D --force ubuntu-mate-15.10-desktop-powerpc.iso /dev/sdx

If you want to make a bootable USB with Windows try [Rufus](https://rufus.akeo.ie/).

<div class="bs-component">
    <div class="jumbotron">
        <h1>Hardware requirements?</h1>
        <p>Ubuntu MATE has modest hardware requirements.</p>
        <a href="/about/" class="btn btn-primary btn-lg">Learn more</a>
        </p>
    </div>
</div>

## Raspberry Pi 2

We are delighted to provide a ready to run image for the [Raspberry
Pi](http://www.raspberrypi.org) 2. You can find out more and download
the image from the [Ubuntu MATE Raspberry Pi page](/raspberry-pi/).
We've also created an [Ubuntu MATE generic root file system and example
build scripts](/armhf-rootfs/) for ARM hackers who'd like bring Ubuntu
MATE to their ARMv7 devices.

<div align="center">
  <a href="/raspberry-pi/"><img src="/images/logos/raspberry-pi.png" /></a><br />
  <b>Ubuntu MATE is also available for the Raspberry Pi 2.</b>
</div>
<br />

## Reporting issues

Please report any issues you may find on the project's bug tracker.

  * [Ubuntu MATE Bug Tracker](https://bugs.launchpad.net/ubuntu-mate)

## Getting involved

Is there anything you can help with or want to be involved in? Maybe
you just want to discuss your experiences or ask the maintainers some
questions. Please [come and talk to us](/community/).

<script>
  // http://netnix.org/2014/04/27/tracking-downloads-with-google-analytics/
  window.onload = function() {
    var a = document.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      if (a[i].href.match(/^https?:\/\/.+\.(bz2|deb|gz|iso|pdf|torrent|xz|zip)$/i)) {
        a[i].setAttribute('target', '_blank');
        a[i].onclick = function() {
          ga('send', 'event', 'Downloads', 'Click', this.getAttribute('href'));
        };
      }
    }
  }
</script>
