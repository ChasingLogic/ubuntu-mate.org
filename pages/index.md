<!--
.. title: Ubuntu MATE
.. slug: index
.. date: 2014-06-10 23:01:09 UTC
.. tags: Ubuntu,MATE
.. link:
.. description:
.. type: text
-->

<img class="centered" src="/images/Ubuntu-MATE-Love.png" alt="Ubuntu MATE" />

<div class="bs-component">
    <div class="jumbotron">
        <h1>For a retrospective future</h1>
        <p>A community developed <a href="http://www.ubuntu.com/" target="_blank">Ubuntu</a> based
        operating system that beautifully integrates the <a href="http://mate-desktop.org" target="_blank">MATE</a> desktop.</p>
        <a href="/about/" class="btn btn-primary btn-lg">Learn more</a>
        </p>
    </div>
</div>

# Screenshots

Here are a few screenshots of Ubuntu MATE to give you a feel for how it looks.

<!-- Outer wrapper for presentation only, this can be anything you like -->
<div align="center">
<div id="banner-fade">
  <!-- start Basic Jquery Slider -->
  <ul class="bjqs">
    <li><a class="image-reference" href="/gallery/Screenshots/00_SYSLINUX.png"><img src="/gallery/Screenshots/00_SYSLINUX.png" title="Ubuntu MATE SYSLINUX Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/01_GRUB.png"><img src="/gallery/Screenshots/01_GRUB.png" title="Ubuntu MATE GRUB Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/02_PLYMOUTH.png"><img src="/gallery/Screenshots/02_PLYMOUTH.png" title="Ubuntu MATE Plymouth Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/03_LIGHTDM.png"><img src="/gallery/Screenshots/03_LIGHTDM.png" title="Ubuntu MATE Login Manager"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/04_DESKTOP.png"><img src="/gallery/Screenshots/04_DESKTOP.png" title="Ubuntu MATE Default Desktop"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/05_ABOUT.png"><img src="/gallery/Screenshots/05_ABOUT.png" title="About MATE"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/08_VIDEOS.png"><img src="/gallery/Screenshots/08_VIDEOS.png" title="Ubuntu MATE Video Player"></a></li>
  </ul>
  <!-- end Basic jQuery Slider -->
</div>
<!-- End outer wrapper -->
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bjqs-1.3.min.js"></script>
<script>
    jQuery(document).ready(function($) {
    $('#banner-fade').bjqs({
        width : 720,
        height : 480,
        responsive : true,
        usecaptions : false
    });
});
</script>

## Raspberry Pi 2

We are delighted to provide a ready to run image for the [Raspberry Pi](http://www.raspberrypi.org) 2.
You can find out more and download the image from the [Ubuntu MATE Raspberry Pi page](/raspberry-pi/).
We've also created an [Ubuntu MATE generic root file system and example build
scripts](/armhf-rootfs/) for ARM hackers who'd like bring Ubuntu MATE to their
ARMv7 devices.

<div align="center">
  <a href="/raspberry-pi/"><img src="/images/logos/raspberry-pi.png" /></a><br />
  <b>Ubuntu MATE is also available for the Raspberry Pi 2.</b>
</div>
<br />

## Sponsors

Ubuntu MATE is kindly supported by [these fine sponsors and our awesome Patrons](/sponsors/):

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://entroware.com"><img class="centered" src="/assets/img/sponsors/entroware.png" alt="Entroware" /></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="http://www.libretrend.com/en/"><img class="centered" src="/assets/img/sponsors/libretrend.png" alt="LibreTrend"/></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://www.first-colo.com/"><img class="centered" src="/assets/img/sponsors/firstcolo.png" alt="First-Colo"/></a>
    </div>
  </div>
</div>
