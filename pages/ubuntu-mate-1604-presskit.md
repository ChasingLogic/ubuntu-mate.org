<!--
.. title: Ubuntu MATE 16.04 Press Kit
.. slug: ubuntu-mate-1604-presskit
.. date: 2015-10-18 20:00:00 UTC
.. tags: Ubuntu,MATE,Press Kit,draft
.. link:
.. description:
.. type: text
-->

With Ubuntu MATE 16.04 due for release on April 21st 2016 this page
will help you better understand what Ubuntu MATE is and how its been
improved since the 15.10 release.

## Introduction

Ubuntu MATE is an community developed [Ubuntu](http://www.ubuntu.com)
based operating system that beautifully integrates the [MATE
Desktop](http://mate-desktop.org) and offers a stable, easy-to-use
operating system with a configurable desktop environment. Ideal for
those who want the most out of their computers and prefer a traditional
desktop metaphor. With modest hardware requirements it is suitable for
modern desktops and laptops, micro computers and older hardware alike.
**Ubuntu MATE is an official member of the Ubuntu family.**

### Objectives

Ubuntu MATE has a number of guiding objectives and goals.

  * Accessible to all, regardless of language and physical ability.
  * Increase both [Ubuntu](http://www.ubuntu.com) and [MATE Desktop](http://mate-desktop.org) user adoption.
  * Ubuntu alternative for computers that aren't powerful enough to run a composited desktop.
  * First choice Ubuntu platform for remote workstation solutions such as [X2Go](http://x2go.org).
  * Recreate the halcyon days of Ubuntu for users who prefer a traditional desktop metaphor.
  * Use themes and artwork similar to Ubuntu so that Ubuntu MATE is immediately familiar.
  * When possible contribute to [Debian](http://www.debian.org) so both the Debian and Ubuntu communities benefit.
  * Software selection will favour functionality and stability over lightness and whimsy.

## New in Ubuntu MATE 16.04

Here is a run down of some of the new features in Ubuntu MATE 16.04:

  * Migrate to MATE Desktop 1.12
  * ...and many other minor improvements and bug fixes.

You can access the complete Ubuntu MATE 16.04 release notes from the
URLs below:

  * [Ubuntu MATE 16.04 Final Release](https://ubuntu-mate.org/blog/ubuntu-mate-xenial-final-release).
  * [Ubuntu MATE 16.04 Beta 2](https://ubuntu-mate.org/blog/ubuntu-mate-xenial-beta2/).
  * [Ubuntu MATE 16.04 Beta 1](https://ubuntu-mate.org/blog/ubuntu-mate-xenial-beta1/).
  * [Ubuntu MATE 16.04 Alpha 2](https://ubuntu-mate.org/blog/ubuntu-mate-xenial-alpha2/).
  * [Ubuntu MATE 16.04 Alpha 1](https://ubuntu-mate.org/blog/ubuntu-mate-xenial-alpha1/).

### Screenshots

Here are a few screenshots of Ubuntu MATE to give you a feel for how it looks.

<!-- Outer wrapper for presentation only, this can be anything you like -->
<div align="center">
<div id="banner-fade">
  <!-- start Basic Jquery Slider -->
  <ul class="bjqs">
    <li><a class="image-reference" href="/gallery/Screenshots/00_SYSLINUX.png"><img src="/gallery/Screenshots/00_SYSLINUX.png" title="Ubuntu MATE SYSLINUX Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/01_GRUB.png"><img src="/gallery/Screenshots/01_GRUB.png" title="Ubuntu MATE GRUB Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/02_PLYMOUTH.png"><img src="/gallery/Screenshots/02_PLYMOUTH.png" title="Ubuntu MATE Plymouth Theme"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/03_LIGHTDM.png"><img src="/gallery/Screenshots/03_LIGHTDM.png" title="Ubuntu MATE Login Manager"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/04_DESKTOP.png"><img src="/gallery/Screenshots/04_DESKTOP.png" title="Ubuntu MATE Default Desktop"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/05_ABOUT.png"><img src="/gallery/Screenshots/05_ABOUT.png" title="About MATE"></a></li>
    <li><a class="image-reference" href="/gallery/Screenshots/08_VIDEOS.png"><img src="/gallery/Screenshots/08_VIDEOS.png" title="Ubuntu MATE Video Player"></a></li>
  </ul>
  <!-- end Basic jQuery Slider -->
</div>
<!-- End outer wrapper -->
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bjqs-1.3.min.js"></script>
<script>
    jQuery(document).ready(function($) {
    $('#banner-fade').bjqs({
        width : 720,
        height : 480,
        responsive : true,
        usecaptions : false
    });
});
</script>

You can access full resolution images from the URL below:

  * <https://ubuntu-mate.org/gallery/Screenshots/>

## Community

The Ubuntu MATE project was founded in June 2014 and since then the
website has had over x.y million unique visitors resulting in z million
page views.

  * Ubuntu MATE **15.10 for i686, amd64, PowerPC** has been downloaded **~x00,000** times.
  * Ubuntu MATE **15.10 for Raspberry Pi 2 has been downloaded ~y00,000** times.
  * **~3,000** members [Ubuntu MATE on Google+](https://plus.google.com/communities/108331279007926658904)
  * **~2,500** member [Ubuntu MATE on Facebook](https://www.facebook.com/UbuntuMATEedition/)
  * **~1,300** followers of [Ubuntu MATE on Twitter](https://twitter.com/ubuntu_mate)
  * **~75** [Patrons](https://www.patreon.com/ubuntu_mate) and **~2870 donations via PayPal**.

Ubuntu MATE is made by [these fine people](/team/).

### Funding

Ubuntu MATE is proud of the community that has grown around the
project. The Ubuntu MATE community fully funds the hosting and bandwidth
costs of the project, including our dedicated community site.

  * [Ubuntu MATE Community](https://ubuntu-mate.community/)

### Supporting Open Source

The donations contributed by the community has started to generate a
surplus which we are using to support other Open Source projects upon
which Ubuntu MATE relies. Since we started supporting others projects
in February 2015 **we have donated a just over $1869** (taking currency
variations into account) to the following projects:

  * [Debian](http://www.debian.org)
  * [Folder Color](http://foldercolor.tuxfamily.org/)
  * [Geany](http://www.geany.org/)
  * [Gufw](http://gufw.org/)
  * [MATE Desktop](http://mate-desktop)
  * [OpenBSD](http://www.openbsd.org)
  * [Plank](http://wiki.go-docky.com/index.php?title=Plank:Introduction)
  * [Tilda](https://github.com/lanoxx/tilda)
  * [TLP](http://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html)
  * [Transmission](http://www.transmissionbt.com/)
  * [Ubuntu](http://www.ubuntu.com)
  * [VLC](http://www.videolan.org)

### Summary of donations

We post an overview of how the donations were used each month.

  * [2014 November](/blog/ubuntu-mate-november-2014-supporters/)
  * [2014 December](/blog/ubuntu-mate-december-2014-supporters/)
  * [2015 January](/blog/ubuntu-mate-january-2015-supporters/)
  * [2015 February](/blog/ubuntu-mate-february-2015-supporters/)
  * [2015 March](/blog/ubuntu-mate-march-2015-supporters/)
  * [2015 April](/blog/ubuntu-mate-april-2015-supporters/)
  * [2015 May](/blog/ubuntu-mate-may-2015-supporters/)
  * [2015 June](/blog/ubuntu-mate-june-2015-supporters/)
  * [2015 July](/blog/ubuntu-mate-july-2015-supporters/)
  * [2015 August](/blog/ubuntu-mate-august-2015-supporters/)
  * [2015 September](/blog/ubuntu-mate-september-2015-supporters/)
  * [2015 October](/blog/ubuntu-mate-october-2015-supporters/)

## Hardware

### Partnerships

Ubuntu MATE has hardware partnerships [Entroware](https://www.entroware.com)
and [LibreTrend](http://www.libretrend.com/en/) who both offer hardware
with the option to have it delivered with Ubuntu MATE pre-installed.

<div class="row">
  <div class="col-lg-6">
    <div class="well bs-component">
    <a href="https://entroware.com"><img class="centered" src="/images/sponsors/entroware.png" alt="Entroware" /></a>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="well bs-component">
    <a href="http://www.libretrend.com/en/"><img class="centered" src="/images/sponsors/libretrend.png" alt="LibreTrend"/></a>
    </div>
  </div>
</div>

### Raspberry Pi 2

<div align="center">
  <a href="/raspberry-pi/"><img src="/images/logos/raspberry-pi.png" /></a><br />
  <b>Ubuntu MATE 15.10 is also available for the Raspberry Pi 2.</b>
</div>
<br />

We are also delighted to provide a ready to run image for the
[Raspberry Pi](http://www.raspberrypi.org) 2 which has been [improved
based on the Raspberry Pi
community](/blog/ubuntu-mate-wily-for-raspberry-pi-2-is-coming/). We've
also created an [Ubuntu MATE generic root file system and example build
scripts](/armhf-rootfs/) for ARM hackers who'd like bring Ubuntu MATE
to their ARMv7 devices.

You grab the download and find out more from the [Ubuntu MATE Raspberry Pi page](/raspberry-pi/).

### Requirements

<div class="row">
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active">Minimum</a>
        <a class="list-group-item">Pentium III 750-megahertz</a>
        <a class="list-group-item">512 megabytes (MB) of RAM</a>
        <a class="list-group-item">8 gigabytes (GB) of available space on the hard disk</a>
        <a class="list-group-item">Bootable DVD-ROM drive</a>
        <a class="list-group-item">Keyboard and Mouse (or other pointing device)</a>
        <a class="list-group-item">Video adapter and monitor with 1024 x 768 or higher resolution</a>
        <a class="list-group-item">Sound card</a>
        <a class="list-group-item">Speakers or headphones</a>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active">Recommended</a>
        <a class="list-group-item">Core 2 Duo 1.6-gigahertz</a>
        <a class="list-group-item">2 gigabytes (GB) RAM</a>
        <a class="list-group-item">16 gigabytes (GB) of available space on the hard disk</a>
        <a class="list-group-item">Bootable USB flash drive</a>
        <a class="list-group-item">Keyboard and Mouse (or other pointing device)</a>
        <a class="list-group-item">3D capable video adapter and wide screen monitor with 1366 x 768 or higher resolution</a>
        <a class="list-group-item">Sound card</a>
        <a class="list-group-item">Speakers or headphones</a>
      </div>
    </div>
  </div>
</div>
