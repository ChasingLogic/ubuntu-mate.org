<!--
.. title: Ubuntu MATE Sponsors
.. slug: sponsors
.. date: 2015-05-13 20:32:42 UTC
.. tags: Ubuntu,MATE,sponsors,donate
.. link:
.. description: Ubuntu MATE sponsors and patrons.
.. type: text
.. author: Martin Wimpress
-->

Ubuntu MATE is kindly supported by these fine sponsors and our awesome Patrons.

## Commercial sponsors

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://entroware.com"><img class="centered" src="/images/sponsors/entroware.png" alt="Entroware" /></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="http://www.libretrend.com/en/"><img class="centered" src="/images/sponsors/libretrend.png" alt="LibreTrend" /></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://www.first-colo.com/"><img class="centered" src="/images/sponsors/firstcolo.png" alt="First-Colo" /></a>
    </div>
  </div>
</div>

## Patrons

Many thanks to the following people for becoming Ubuntu MATE patrons who collectively donated **$492.0** this month.

### The following Patrons contribute $10, or more, every month.

<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>Patrons</th>
      <th>Joined</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Poul LeBlanc</td>
      <td>2015-01-17 19:21:40</td>
    </tr>
    <tr>
      <td>Robert Boudreau</td>
      <td>2015-03-08 21:01:28</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/Entroware">Entroware</a></td>
      <td>2015-03-07 03:44:29</td>
    </tr>
    <tr>
      <td>Davide Monge</td>
      <td>2015-03-11 22:57:13</td>
    </tr>
    <tr>
      <td>Steve J. Radonich IV</td>
      <td>2014-11-09 10:44:55</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/spazmaticcelery">SpazzyC</a></td>
      <td>2014-11-10 20:32:27</td>
    </tr>
    <tr>
      <td>Dave Hills</td>
      <td>2015-05-14 18:12:54</td>
    </tr>
    <tr>
      <td>David Rush</td>
      <td>2015-06-20 17:04:23</td>
    </tr>
    <tr>
      <td>Mike Kelley</td>
      <td>2015-06-20 14:08:38</td>
    </tr>
    <tr>
      <td>David Kerr</td>
      <td>2015-06-17 19:42:10</td>
    </tr>
    <tr>
      <td>Brian</td>
      <td>2015-07-07 23:35:19</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/shannonodam">Shannon Odam</a></td>
      <td>2015-08-18 04:09:17</td>
    </tr>
    <tr>
      <td>Michael Vanier</td>
      <td>2015-07-06 06:31:31</td>
    </tr>
    <tr>
      <td>Paul</td>
      <td>2015-09-21 03:23:21</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/htpcBeginner">htpcBeginner.com</a></td>
      <td>2015-10-02 13:01:39</td>
    </tr>
  </tbody>
</table>
<br />

### The following Patrons also contribute every month.

<small>David Hollings, Zora Saad, <a href="https://twitter.com/ChrisLAS">Tech Talk Today</a>, Robert Meineke, Joe, <a href="https://twitter.com/nadrimajstor">Ivan Pejić</a>, <a href="https://twitter.com/dirkjanvdhoorn">Dirk-Jan van der Hoorn</a>, DoctorKraz, Sergio Rivera, Silas Wulff, Jack Blakely, Adrian Evans, Matt Hartley, M Hoppes, Antoine Mate Messiah, Daniel Neilson, Scott Petty, david van Dyk, Mark Boadey, Michael McGuire, <a href="https://twitter.com/rdp5008">Russell Pate</a>, Ricardo Gerena, Trooper_Ish, Peter Mergaerts, Christian Setla, Gheorghe Kobori, John Jeffers P.Eng., <a href="https://twitter.com/drgroovestarr">Jarrod Ryan Worlitz</a>, Ryan Carter, Brent Kincer, Richard Arnold, Christopher atkins, Stephen Donovan, Jason Hyder, sfarber5300, Chris Gardiner-Bill, Patrik Nilsson, <a href="https://twitter.com/ifollowyou">Cato Gaustad</a>, Atreju, <a href="https://twitter.com/taksuyu">Tak Suyu</a>, Specops872, Krishna, Paul Howarth, Jordan Hopkins, <a href="https://twitter.com/ebeyer">Eric</a>, Michael White, Lukasz, <a href="https://twitter.com/magnuslindstrom">gnusd</a>, Darren Hammond, Bevan Thomas, Dow Hurst, Thurman Lewis, Ed, Mike Brannon, <a href="https://twitter.com/abosio">Anthony Bosio</a>, Daron, Bob Wright, Michael Burchfield, <a href="https://twitter.com/jed_reynolds">Jed Reynolds</a>, Eduardo Sanchez, Phillip Stromberg, Gaius, Mandy Tonks, Jason P. Stanford, Graham Moss, </small><br />

### The *Unlucky* Patron!

Darrell Vermilion is a good sport! They have chosen to be the Ubuntu MATE *unlucky* Patron, just so they can see their name in flashing lights.

<div align="center">
<h2><blink>Darrell Vermilion</blink><h2>
</div>

<div class="bs-component">
    <div class="jumbotron">
        <h1>Monthly supporter</h1>
        <p>Become a monthly supporter at <a href="http://www.patreon.com/ubuntu_mate">Patreon</a>.
        Patrons get exclusive project updates, invites to live video conferences with the Ubuntu
        MATE developers and discounts on some Ubuntu MATE merchandise.</p>
        <a href="http://www.patreon.com/ubuntu_mate" class="btn btn-primary btn-lg">Become a Patron</a>
        </p>
    </div>
</div>

<script type="text/javascript">
  setInterval(function(){
      $('blink').each(function(){
        $(this).css('visibility' , $(this).css('visibility') === 'hidden' ? '' : 'hidden')
      });
    }, 250);
</script>
