<!-- 
.. title: IRC
.. slug: irc
.. date: 2014-06-10 23:01:09 UTC
.. tags: Ubuntu,MATE,IRC
.. link: 
.. description: 
.. type: text
.. author: Martin Wimpress
-->

This IRC web client is pre-configured so you can jump into our `#ubuntu-mate` channel on 
[Freenode](https://freenode.net/). Most of the Ubuntu MATE team are in here but they have
real lives too. If you have a question, do ask. **However, it may take a while for someone
to reply. Just be patient and don't disconnect right away.**

<iframe src="https://kiwiirc.com/client/chat.freenode.net:+6697/?nick=mate%7C?#ubuntu-mate" style="border:0; width:100%; height:500px;"></iframe>
             
